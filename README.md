## Installation

Clone project
```
$ git clone [url]
```

Create database with Collation utf8_general_ci.


Run commands
```
$ cd [project_folder]
$ mv .env.example .env
```

In .env file change :

MAIL_DRIVER=smtp

MAIL_HOST=smtp.gmail.com

MAIL_PORT=587

MAIL_USERNAME=XXX@gmail.com

MAIL_PASSWORD=XXX

MAIL_ENCRYPTION=tls


In config\database.php change 'mysql' => 'database' => "NEW_DB_NAME"

Run commands
```
$ composer install
$ php artisan key:generate
$ php artisan migrate
$ php artisan serve
```

To check if it works, in browser open http://127.0.0.1:8080/

If config got cached
```
$ php artisan config:clear
```
