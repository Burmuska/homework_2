<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;
use Illuminate\Support\Facades\Input;

class VerifiedEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $input = Input::all();
        
        $userStatus = User::select('email_verified_at')
                    ->where(['email' => $value])
                    ->first();

        if ( $userStatus && is_null($userStatus->email_verified_at) ) {
            // If verification login, allow it
            if ( isset($input['verification']) && $input['verification'] === '1' ) {
                return true;
            }
            return false;
        } else return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.verifiedEmail');
    }
}
