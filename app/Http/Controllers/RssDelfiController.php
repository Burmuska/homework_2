<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RssDelfiController extends Controller
{
    protected $rssLink = 'https://www.delfi.lv/rss/?channel=delfi';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get Delfi RSS xml feeds
    */
    public function index () 
    {
        $data = simplexml_load_file($this->rssLink, 'SimpleXMLElement', LIBXML_NOCDATA);

        $result = [];
        if ( $data && !empty($data->channel->item) ) {
            foreach ( $data->channel->item as $info ) {
                // Change xml object to simple object
                $img = ( $info->children( 'media', True ) ? $info->children( 'media', True )->content->attributes()['url'] : '');
                $info = json_encode($info);
                $info = json_decode($info);

                $result[] = [
                    'title' => $info->title,
                    'link' => $info->link,
                    'description' => $info->description,
                    'img' => $img,
                    'date' => date('d.m.Y H:i', strtotime($info->pubDate)),
                ];
            }
        }

        return view('news/feeds', [
            'news' => $result
        ]);
    }
}
