<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::post('/uniqueEmail', 'Auth\RegisterController@validateEmailUnique');
Route::get('/loginVerify', 'Auth\LoginController@showLoginFormVerify')->name('loginVerify');
Route::get('/waitingVerification', function () {
    return view('auth/verify');
});

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/', 'RssDelfiController@index')->middleware('verified');