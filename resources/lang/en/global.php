<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'emptyNews' => 'There is a problem with news portal connection. Please, try again later!',
    'page.news' => 'Delfi news',

];
