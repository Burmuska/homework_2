@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h3> {{ __('global.page.news') }}</h3>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-10 col-md-10 col-sm-12 col-sm-12">

            @if ( !empty($news) )
                @foreach ($news as $feed)
                    <div class="card">
                        <div class="card-horizontal">
                            <div class="img-square-wrapper pt-4 pl-4">
                                @if ( !empty($feed['img']) )
                                    <img class="" src="{{$feed['img']}}" alt="{{$feed['title']}}">
                                @endif
                                </div>
                            <div class="card-body">
                                <h5 class="card-title">{{$feed['title']}}</h5>
                                <p class="card-text">{!! $feed['description'] !!}</p>
                                <a href="{{$feed['link']}}" target="_blank" class="card-link">Read more</a>
                                <footer class="blockquote-footer">{{$feed['date']}}</footer>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning text-center" role="alert">
                    {{ __('global.emptyNews') }}
                </div>
            @endif

        </div>
    </div>
</div>
@endsection